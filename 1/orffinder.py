##############
# Exercise 2.5
##############

# You can use the supplied test cases for your own testing. Good luck!


def complementary(sequence):
    myDict = {'A': 'T',
              'G': 'C',
              'T': 'A',
              'C': 'G'}

    out = list(sequence)

    for i, a in enumerate(sequence):
        out[i] = myDict[a]

    return ''.join(out)


def get_aa_from_codon(codon):

    aa_encodings = {
        'F': ['TTT', 'TTC'],
        'L': ['TTA', 'TTG', 'CTT', 'CTC', 'CTA', 'CTG'],
        'I': ['ATT', 'ATC', 'ATA'],
        'M': ['ATG'],
        'V': ['GTT', 'GTC', 'GTA', 'GTG'],
        'S': ['TCT', 'TCC', 'TCA', 'TCG', 'AGT', 'AGC'],
        'P': ['CCT', 'CCC', 'CCA', 'CCG'],
        'T': ['ACT', 'ACC', 'ACA', 'ACG'],
        'A': ['GCT', 'GCC', 'GCA', 'GCG'],
        'Y': ['TAT', 'TAC'],
        'H': ['CAT', 'CAC'],
        'Q': ['CAA', 'CAG'],
        'N': ['AAT', 'AAC'],
        'K': ['AAA', 'AAG'],
        'D': ['GAT', 'GAC'],
        'E': ['GAA', 'GAG'],
        'C': ['TGT', 'TGC'],
        'W': ['TGG'],
        'R': ['CGT', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'],
        'G': ['GGT', 'GGC', 'GGA', 'GGG'],
        'X': ['TAA', 'TAG', 'TGA']
    }

    aa = ''

    for key, value in aa_encodings.items():
        if codon in value:
           aa = key

    return aa


def codons_to_aa(orf):

    return ''.join([get_aa_from_codon(orf[i:i+3]) for i in range(0, len(orf), 3)])

def find_orfs(genome, reverse=False):
    orfs = []
    org_len = int(len(genome)/2)

    start_codons = []
    stop_codons = []
    used_stop_codons = []

    # Find start codon
    for i in range(len(genome) - 2):
        if genome[i:i + 3] == 'ATG':
            start_codons.append(i)
        if genome[i:i + 3] in ['TAA', 'TAG', 'TGA']:
            stop_codons.append(i)

    for i in range(1, len(stop_codons)+1):

        stop_index = stop_codons[-i]

        mapped_stop_index = (stop_index + 2) % org_len


        if mapped_stop_index not in used_stop_codons:

            prev_stop_index = 0
            # Find previous stop index that is codon aligned
            for i_prev in range(i+1, len(stop_codons)+1):
                if (stop_index - stop_codons[-i_prev]) % 3 == 0:
                    prev_stop_index = stop_codons[-i_prev]
                    break

            mapped_prev_stop_index = (prev_stop_index+2) % org_len

            for start_index in start_codons:

                mapped_start_index = start_index % org_len

                if start_index < stop_index and start_index > prev_stop_index:
                    if ((stop_index - start_index) % 3) == 0 and (stop_index - start_index) >= 34 * 3:
                        if reverse:
                            orfs.append((org_len - mapped_start_index - 1, org_len - mapped_stop_index - 1,
                                        codons_to_aa(genome[start_index:stop_index + 2]), True))

                        else:

                            orfs.append((mapped_start_index, mapped_stop_index,
                                         codons_to_aa(genome[start_index:stop_index + 2]), False))

                        break

            used_stop_codons.append(mapped_stop_index)







    # for start_index in start_codons_reordered:
    #     for stop_index in stop_codons:
    #         # Find the next stop codon
    #         if stop_index > start_index and ((stop_index - start_index) % 3) == 0:
    #
    #             mapped_stop_index = (stop_index + 2) % org_len
    #
    #             # Only accept if the sequence is long enough
    #             if (stop_index - start_index) >= 34 * 3 and mapped_stop_index not in used_stop_codons:
    #                 if reverse:
    #                     orfs.append((org_len - start_index - 1, org_len - mapped_stop_index - 1,
    #                                 codons_to_aa(genome[start_index:stop_index + 2]), True))
    #
    #                 else:
    #
    #                     orfs.append((start_index, mapped_stop_index,
    #                                  codons_to_aa(genome[start_index:stop_index + 2]), False))
    #
    #                 used_stop_codons.append(mapped_stop_index)
    #
    #             break

    return orfs

def get_orfs(in_genome):

    for x in in_genome:
        if not x in ['T', 'G', 'C', 'A']:
            raise TypeError

    genome = in_genome + in_genome

    orfs = find_orfs(genome)

    reverse_complement = complementary(genome[::-1])

    orfs.extend(find_orfs(reverse_complement, reverse=True))

    return orfs


get_orfs('TTACTTGATACTATTGATTAAAGCGGGGACAAAATTTGCAGAATTTAGATAAGAAAAAACCCCTGTTATCGGACAGTTTGGCGACCGGTGATAACAAGGGTTTTGCATCTCCCAAAGGAGATCAACATAGGGATAGAATAACACGTTTTGGCATTTTGAAACATAGATCGAAGCAACAAGAAAACTATTTATTTTCGTTAGCTAAGATTAAAGAAAATTATCATGCCGATGTAAAAAACGATGAATCTATTCGCGCCATGAAAACTGCCCAAAAATTAAATGGGTGCGGTAATTTTCTTCTATTCAAAAATTTTTACACCATTAATCAAATTAAACTCGCCAAGTTCCAAGCTTGTAGTGAGCATTTGTTATGTCCGTTTTGTGCTGGTATTAGAGCTTCTAAGGCAATTCAAAAATACTCTGAGCGTGTTGATCAAGTCTTATCTGAAAATCCTCGTTTAAAGCCCGTTATGATCACGTTTACGGTTAAAAATGGGGTAGACCTAGGGGAACGGTTCACCCATCTTATAAAATCGTTTAGAACGCTTATAGAGCGTCGTAGGGACTATATTAAAAAAGGGCGTGGCTTTAATGAATTTTGCAAAATTAATGGTGCGATGTATTCATATGAGAATACTTACAATGAAAAAACTAATGAATGGCATCCTCATATTCATATGTTTGCACTTTTGGATGATTGGATAGATCAGGATGAATTGTCTCAATATTGGCAATCCATTACTGGGGACTCTATGGTCGTTGATATTCGTAGAGCCAAAAAACAAAAAGACTTAGGCTATTCAGGTGCTGCTGCTGAAGTCTGTAAATATGCTCTCAAATTTGGTGATCTTTCTGTAGAAAAGACTTGGGAAGCTTTCAAAGTTTTGAAAGGTAAGCGATTAAGTGGGGCTTTTGGATCTCTTTGGGGCGTGAAAATTCCTGAATCATTGATAGATGATCTTCCAGACGATTCTGATTTACCTTATTTAGAAATGATTTATAAGTTCGTCTTTTCTAAGAAGTCTTATTACGATTTACAACTTACTCGTCATGTCGAACCTACAGGTAAGGACGACGCCGACGAGCTTCGAGGAGAAGAAGGACGCAACCTGTTGGTGAGCATGGACGGGCGAGGAGCGAGCGACGCTGGGAGGGCCCGCACTGGCGCGCTAGCCCCGCAGCACGGACGAAAAAAACAACACTGGCAAATTCCACCAGTTACTCGTGTTCGGGTTCGGAAGCGAATCCGAAGATGGGACGGATATTTATGTGTCTTACATTTATAGTTTTTTAACTAGTTAATAATACTAGTAAGTCGGTGAAATCAGGGAGTGCCATCCCCGATTTCACCTAACCACAACGTACTGTCAAGGAGTACTTATCATGGCTAACGCGAATACTACTGTTTTAGAACTTTTAGGCAAACAAGTTTCTTTTGTTTATGTTCTTAAGTCTGATTCAGATGAATATTCTTTCACCTGTTCTGGTGTTGTGACTGATGTAATTATCAGTCTTAACTCTGAACCACAGCTCTCTGTTGATAACGGGGATTTTTATATTTATTCGGATTTAAAGGATTTCTTTATTAAGTCTGAATAAGGGTCTCTTGGTATATTTACGAAATCTCATGCCACGCTTGAAACTTAAGGGTTTCAGGCGTGTTTTTTTATAGTTTCTTGAGAAACGCGTAAGCGTGCATTACTGAAAAAACACCTAAATCTTAGAGTGGGTGTCTACCCACGAACTGACATAAAACGATTTAAAAACTTTCCTCTCTGATTACAAGTTCCCTTTAGCCTAAGAACGGGAGTTACCGAGCTTTGCTCGCCGTGATATTGAATAGATTTTAATTTTGATATATAAATGGAATCACAAAATGATTCCTTTTATATATAAGGTAAGATTTATGAGCAATCCATCAAAATTAGATACTCTTGTTGCAGAGATTAGTGGTACTGCTGAATCTGAATATATTGGTCTTTATAAGCAGATTCCTTTTCGTGTAAGACTTCCCCTTTTTGCACGTTTATCAGCACTTCATACCGTCCAAAATTCCCATAATAAAACTCCGCGTAATGCGCTGTTGAATGACTTACTTGAAATCGCCATGGATCAAGTTATGTCTAAATTAGATGAGGAAACTTTGGAAGGTCTTGGCCATTTAGAAGCTCAATATTATGAAGAGCTTTCTCAATATGATTCTGGAGAACTTTCAGATGATTAGAAAACCACGAATTATAAGTAACACTCCTATGGAAAGCCTGGATGAAGCATGTACTTACTTTAGACAGGTTCTCCAAGAACATTTAGATAAAAAAAGACAAATTACATTTTTTACCATTGCTATGGTTGATGGTCGTTTGAAGTTAATTGAGGGGATACCAGAATGATTAAGCTTGAAGGCACAGTTTTAAATACATACCACTTAGATGGTGGGACTAATAAAAAGGGCGAAGAATATGAAGCTCGAGATAAAGTCCAACTTCTAGGTTCGTTGGAGCTTCCCAACGGACAAATTAAAAATGAATTAATCGACCTTACTGTCGAAGATTCTCGTATTTATGATGATTTCAAAAATAAGCTTATTAGCATCAGTTGTGGTGCTATGGCTGTTGGTCGTAACGTTATTTTTTATGTTCGAAAAGGTGCGAAACCTGTTTTAGCAGATCACTTATGATTTTCCCTAAAAAACATCGAGTTAGCGAAGCGTCTCGATGTTTTTTCTT')


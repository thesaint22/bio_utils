##############
# Exercise 2.7
##############


def isCharged(aa):
    return isPositivelyCharged(aa) or isNegativelyCharged(aa)

def isPositivelyCharged(aa):
    return aa in 'RHK'

def isNegativelyCharged(aa):
    return aa in 'DE'

def isHydrophobic(aa):
    return aa in 'AVILMFYW'

def isAromatic(aa):
    return aa in 'HFWY'

def isPolar(aa):
    return aa in 'RNDQEHKSTY'

def isProline(aa):
    return aa == 'P'

def containsSulfur(aa):
    return aa in 'CM'

def isAcid(aa):
    return aa in 'DE'

def isBasic(aa):
    return aa in 'RHK'


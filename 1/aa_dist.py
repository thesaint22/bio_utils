##############
# Exercise 2.6
##############

from collections import Counter

class AADist:
    """
    The class provides a method to read fasta files and to calculate certain statistics on the read sequences.
    """
    
    def __init__(self, filepath):
        self.__sequences = []
        self.read_fasta(filepath)
        

    def get_counts(self):
        return len(self.__sequences)
        pass

    def get_total_length(self):
        return sum([len(body) for (header, body) in self._AADist__sequences])

    def get_average_length(self):

        return self.get_total_length() / float(self.get_counts())

    def aa_dist(self):

        cnt = Counter()

        for aa in self.__sequences:
            cnt[aa] += 1

        return cnt

    def read_fasta(self, path):

        this_header = ''
        this_body = ''

        with open(path, 'r') as f:
            for line in f:
                if line[0] == '>':
                    this_header = line[:-1]
                elif line == '\n':
                    self.__sequences.append((this_header, this_body))
                    this_header = ''
                    this_body = ''
                else:
                    this_body = this_body + line[:-1].strip('* ')

        if this_header != '':
            self.__sequences.append((this_header, this_body))

    def get_abs_frequencies(self):

        cnt = Counter()

        for body in [body for (header, body) in self.__sequences]:
            for aa in body:
                cnt[aa] += 1

        return cnt

    def get_av_frequencies(self):

        cnt = self.get_abs_frequencies()
        total_length = self.get_total_length()

        for key, value in cnt.items():
            cnt[key] = value / float(total_length)

        return cnt



dist =  AADist('tests/tests.fasta')

print(dist.get_counts())

print(dist.get_average_length())

print(dist.get_abs_frequencies())

pass

import numpy as np
import json
from pathlib import Path
from itertools import compress

import re
from collections import Counter


"""
ATTENTION: Use the following dictionaries to get the correct index for each
           amino acid when accessing any type of matrix (PSSM or substitution
           matrix) parameters. Failure to do so will most likely result in not
           passing the tests.
"""
ALPHABET = 'ACDEFGHIKLMNPQRSTVWY'
AA_TO_INT = {aa: index for index, aa in enumerate(ALPHABET)}
INT_TO_AA = {index: aa for index, aa in enumerate(ALPHABET)}

# Get all possible words
POSSIBLE_WORDS = []
for i in range(20):
    for j in range(20):
        for k in range(20):
            POSSIBLE_WORDS.append(INT_TO_AA[i] + INT_TO_AA[j] + INT_TO_AA[k])

w = 3


class BlastDb:

    def __init__(self):
        """
        Initialize the BlastDb class.
        """
        self.sequences = []
        self.cache = {}

    def add_sequence(self, sequence):
        """
        Add a sequence to the database.

        :param sequence: a protein sequence (string).
        """
        self.sequences.append(sequence)
        for i in range(len(sequence) - w + 2):
            word = sequence[i:i+3]
            if word not in self.cache:
                self.cache[word] = set()
                self.cache[word].add(sequence)
            else:
                self.cache[word].add(sequence)


    def get_sequences(self, word, as_list=True):
        """
        Return all sequences in the database containing a given word.

        :param word: a word (string).

        :return: List with sequences.
        """
        # if word not in self.cache:
        #     self.cache[word] = list(compress(self.sequences, [word in sequence for sequence in self.sequences]))

        if as_list:
            return list(compress(self.sequences, [word in sequence for sequence in self.sequences]))
        else:
            return self.cache[word]

    def get_db_stats(self):
        """
        Return some database statistics:
            - Number of sequences in database
            - Number of different words in database
            - Average number of words per sequence (rounded to nearest int)
            - Average number of sequences per word (rounded to nearest int)

        :return: Tuple with four integer numbers corrsponding to the mentioned
                 statistics (in order of listing above).
        """
        all_words = set()
        words_per_sequence = []
        word_counts = Counter()

        for i_seq, sequence in enumerate(self.sequences):

            words_per_sequence.append(set([sequence[i: i+w] for i in range(len(sequence) - w + 1)]))
            for word in words_per_sequence[i_seq]:
                word_counts[word] += 1

            all_words.update(words_per_sequence[i_seq])

        # Average number of words per sequence
        avg_words_per_seq = [len(word_list) for word_list in words_per_sequence]
        avg_words_per_seq = round(sum(avg_words_per_seq) / len(avg_words_per_seq))

        # Average number of sequences containing a word
        avg_seqs_per_word = round(sum([word_counts[key] for key in word_counts]) / len(word_counts))

        return tuple([len(self.sequences),
                      len(all_words),
                      avg_words_per_seq,
                      avg_seqs_per_word])

class Blast:

    def __init__(self, substitution_matrix):
        """
        Initialize the Blast class with the given substitution_matrix.

        :param substitution_matrix: 20x20 amino acid substitution score matrix.
        """
        self.sub_matrix = substitution_matrix

    def get_words(self, *, sequence=None, pssm=None, T=11, verbose=False):
        """
        Return all words with score >= T for given protein sequence or PSSM.
        Only a sequence or PSSM will be provided, not both at the same time.
        A word may only appear once in the list.

        :param sequence: a protein sequence (string).
        :param pssm: a PSSM (Lx20 matrix, where L is length of sequence).
        :param T: score threshold T for the words.

        :return: List of unique words.
        """
        words_verbose = {}

        if pssm is None:
            seq_len = len(sequence)
            seq = sequence
            is_pssm = False
        else:
            seq_len = len(pssm)
            seq = pssm
            is_pssm = True


        # Calculate alignment scores for all words at all positions
        for i in range(seq_len - w + 1):
            for word in POSSIBLE_WORDS:
                # Calculate score
                score = 0

                if pssm is None:
                    seq_word = sequence[i: i+w]

                for j in range(len(word)):
                    score += self.get_score(word[j], seq, i+j, is_pssm)

                if score >= T:
                    if i in words_verbose:
                        words_verbose[i].append(word)
                    else:
                        words_verbose[i] = [word]

        if verbose:
            return words_verbose
        else:
            words = []
            for key in words_verbose:
                words.extend([word for word in words_verbose[key]])
            return list(set(words))

    def search_one_hit(self, blast_db, *, query=None, pssm=None, T, X, S):
        """
        Search a database for target sequences with a given query sequence or
        PSSM. Return a dictionary where the keys are the target sequences for
        which HSPs have been found and the corresponding values are lists of
        tuples. Each tuple is a HSP with the following elements (and order):
            - Start position of HSP in query sequence
            - Start position of HSP in target sequence
            - Length of the HSP
            - Total score of the HSP
        The same HSP may not appear twice in the list (remove duplictes).
        Only a sequence or PSSM will be provided, not both at the same time.

        :param blast_db: BlastDB class object with protein sequences.
        :param query: query protein sequence.
        :param pssm: query PSSM (Lx20 matrix, where L is length of sequence).
        :param T: score threshold T for the words.
        :param X: drop-off threshold X during extension.
        :param S: score threshold S for the HSP.

        :return: dictionary of target sequences and list of HSP tuples.
        """
        results = {}

        candidates = self.get_words(sequence=query, pssm=pssm, T=T, verbose=True)

        for i_q, words in candidates.items():

            for word in words:
                targets = blast_db.get_sequences(word, as_list=False)

                for target in targets:

                    # Find where the word appears
                    target_indices = [m.start() for m in re.finditer('(?=' + word + ')', target)]

                    for i_t in target_indices:
                        if pssm is None:
                            hsp = self.get_hsp_one_hit(query, i_q, target, i_t, False, S, X)
                        else:
                            hsp = self.get_hsp_one_hit(pssm, i_q, target, i_t, True, S, X)

                        if hsp is not None:
                            if target in results:
                                if hsp not in results[target]:
                                    results[target].append(hsp)
                            else:
                                results[target] = [hsp]

        return results

    def get_score(self, aa, query, index, is_pssm):
        if is_pssm:
            score = query[index][AA_TO_INT[aa]]
        else:
            score = self.sub_matrix[AA_TO_INT[query[index]]][AA_TO_INT[aa]]

        return score

    def get_hsp_one_hit(self, query, i_q, target, i_t, is_pssm, s, x):
        score = 0
        init_i_q = i_q
        init_i_t = i_t
        start_q = i_q
        start_t = i_t
        stop_q = i_q + 2

        # Get score for word
        for i in range(3):
            score += self.get_score(target[init_i_t+i], query, init_i_q+i, is_pssm)

        best_score = score

        i_q = init_i_q - 1
        i_t = init_i_t - 1

        # Move to left
        while i_q >= 0 and i_t >= 0:
            score += self.get_score(target[i_t], query, i_q, is_pssm)

            if score > best_score:
                best_score = score
                start_q = i_q
                start_t = i_t

            if best_score - score >= x:
                break

            i_q -= 1
            i_t -= 1

        i_q = init_i_q + 3
        i_t = init_i_t + 3
        score = best_score

        # Move to right
        while i_q < len(query) and i_t < len(target):
            score += self.get_score(target[i_t], query, i_q, is_pssm)

            if score > best_score:
                best_score = score
                stop_q = i_q

            if best_score - score >= x:
                break

            i_q += 1
            i_t += 1


        if best_score >= s:
            return tuple((start_q, start_t, stop_q - start_q + 1, int(best_score)))
        else:
            return None

    def search_two_hit(self, blast_db, *, query=None, pssm=None, T=11, X=5, S=30, A=40):
        """
        Search a database for target sequences with a given query sequence or
        PSSM. Return a dictionary where the keys are the target sequences for
        which HSPs have been found and the corresponding values are lists of
        tuples. Each tuple is a HSP with the following elements (and order):
            - Start position of HSP in query sequence
            - Start position of HSP in target sequence
            - Length of the HSP
            - Total score of the HSP
        The same HSP may not appear twice in the list (remove duplictes).
        Only a sequence or PSSM will be provided, not both at the same time.

        :param blast_db: BlastDB class object with protein sequences.
        :param query: query protein sequence.
        :param pssm: query PSSM (Lx20 matrix, where L is length of sequence).
        :param T: score threshold T for the words.
        :param X: drop-off threshold X during extension.
        :param S: score threshold S for the HSP.
        :param A: max distance A between two hits for the two-hit method.

        :return: dictionary of target sequences and list of HSP tuples.
        """
        q_candidates = self.get_words(sequence=query, pssm=pssm, T=T, verbose=True)

        if pssm is None:
            len_query = len(query)
        else:
            len_query = len(pssm)

        hits = self.get_hits(q_candidates, len_query, blast_db)

        results = self.get_results(len_query, hits, A, S, X, query, pssm)

        return results

    def get_results(self, len_query, hits, A, S, X, query=None, pssm=None, ):
        results = {}

        for target in hits:
            len_target = len(target)
            results[target] = []
            # Gehe durch alle Diagonalen

            for i_diag in range(-len_target + 1, len_query):
                diag = np.diagonal(hits[target], i_diag)
                indices = list(np.where(diag==1))[0]

                len_indices = len(indices)

                i_index_l = 0
                i_r = 1
                right_border = -1

                while i_index_l < len_indices - 1:
                    hl = indices[i_index_l]

                    hr = indices[i_index_l + i_r]

                    if (hr - hl > 2) and (hr - hl <= A):
                        candidate = (hl + max(0, i_diag), hl - min(0, i_diag),
                                     hr + max(0, i_diag),
                                     hr - min(0, i_diag)) # (i_q_hl, i_t_hl, i_q_hr, i_t_hr)

                        if pssm is None:
                            hsp = self.get_hsp_two_hit(query, candidate[2], target, candidate[3], False, S, X)
                        else:
                            hsp = self.get_hsp_two_hit(pssm, candidate[2], target, candidate[3], True, S, X)

                        # If HSP is valid
                        if hsp[0] <= (candidate[0] + 2):

                            is_ok = hsp[0] > right_border

                            right_border = hsp[0] + hsp[2]

                            # Add to results if score is okay
                            if is_ok and hsp[3] >= S:
                                results[target].append(hsp)

                    if (i_index_l + i_r) >= len(indices) - 1:
                        i_index_l += 1
                        i_r = 1
                    else:
                        i_r += 1


        results = {k:v for k,v in results.items() if v}

        return results

    def get_hits(self, q_candidates, len_query, blast_db):

        hits = {}

        for i_q, words in q_candidates.items():

            # print(str(i_q) + '/' + str(len_query))

            for word in words:
                targets = blast_db.get_sequences(word, as_list=False)

                for target in targets:

                    len_target = len(target)

                    # indices = [m.start() for m in re.finditer('(?=' + word + ')', target)]
                    indices = list(find_all(target, word))

                    if target not in hits:
                        hits[target] = np.zeros((len_target, len_query))

                    hits[target][indices, i_q] = 1

        return hits

    def get_hsp_two_hit(self, query, i_q, target, i_t, is_pssm, s, x):
        score = 0
        best_score = -1000000
        init_i_q = i_q
        init_i_t = i_t
        start_q = i_q
        start_t = i_t
        stop_q = i_q + 2
        stop_t = i_t + 2

        # Get score for word
        for i in range(3):
            score += self.get_score(target[init_i_t + i], query, init_i_q + i, is_pssm)

        best_score = score

        i_q = init_i_q - 1
        i_t = init_i_t - 1

        # Move to left
        while i_q >= 0 and i_t >= 0:
            score += self.get_score(target[i_t], query, i_q, is_pssm)

            if score > best_score:
                best_score = score
                start_q = i_q
                start_t = i_t

            if best_score - score >= x:
                break

            i_q -= 1
            i_t -= 1

        i_q = init_i_q + 3
        i_t = init_i_t + 3
        score = best_score

        # Move to right
        while i_q < len(query) and i_t < len(target):
            score += self.get_score(target[i_t], query, i_q, is_pssm)

            if score > best_score:
                best_score = score
                stop_q = i_q

            if best_score - score >= x:
                break

            i_q += 1
            i_t += 1

        # if best_score < s:
        #     return None
        # else:
        return tuple((start_q, start_t, stop_q - start_q + 1, int(best_score)))


def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += 1


def compare_blast_results(blast_results, results):
    passed_1 = (len(blast_results) == len(results))
    passed_2 = (set(blast_results) == set(results))

    passed = (passed_1 and passed_2)

    # assert passed, 'Incorrect target sequences returned.'

    for target, hsp_list in results.items():
        blast_hsp_list = blast_results[target]

        passed_1 = (len(blast_hsp_list) == len(hsp_list))
        passed_2 = (set(blast_hsp_list) == set(hsp_list))

        passed = (passed_1 and passed_2)

        assert passed, 'Incorrect HSPs returned.'

def table_list_tuple(data):
    for key, value in data.items():
        data[key] = [tuple(x) for x in value]
    return data

def main():
    with open('tests/blast_test.json') as f:
        data = json.load(f)

        blast_db = BlastDb()

        # blast_db.add_sequence('MVEAIVEFDYQAQHDDELTISVGEVITNIRKEDGGWWEGQINGRRGLFPDNFVREIKKDVKKDLLSNKAPEKPMHDVSSGNSLLSSETILRTNKRGERRRRRCQVAFSYLPQNDDELELKVGDIIEVVGEVEEGWWEGVLNGKTGMFPSNFIKELSGESDELGISQDEQLSKSRPEGFLPASLLPFPAHGAKGKTTFEGTILYRAAPGKTEGHRRYYSLRETTGSESDGGDSSSTKSEGANGTVATAAIQPKKVKGVGFGDIFKDKPIKLRPRSIEVENDFLPVEKTIGKKLPPATSTPDPSKTEMDSRTKTKDYCKVIFPYEAQNDDELTIKEGDIVTLINKDCIDVGWWEGELNGRRGVFPDNFVKLLPSDFDKEGNRPKKPPPPSAPVIKQGAGTTERKHEIKKIPPERPETLPNRTEEKERPEREPKLDLQKPSVPAIPPKKPRPPKTNSLNRPGVLPPRRPERPVGPLTHTRGDSSKIDLAGSTLSGILDKDLSDRSNDIDLEGFDSVISSTEKLSHPTTSRPKATGRRPPSQSLTSSSLSSPDIFDSPSPEEDKEEHISLAHRGIDVSKKTSRTVTISQVSDNKASLPPKPGTMAAASSGPASLSSVASSPMSSSLGTAGQRASSPSLFSAEGKAKTESAVSSQAAIEELKMQVRELRTIIETMKDQQKREIKQLLSELDEEKKIRLRLQMEVNDIKKALQSK')
        # blast_db.add_sequence('MQKAIRLNDGHVVSLGLLAQRDGTRKGYLSKRSSDNPKWQTKWFALLQNLLFYFESDSSSRPSGLYLLEGSICKRMPSPKRGTSSKESDKQHHYFTVNFSNDSQKSLELRTDDSKDCDEWVAAIARASYKILATEHEALMQKYLHLLQVVETEKTVAKQLRQQLEDGEVEIERLKAEIANLIKDNERIQSNQLVAPEDEDSDIKKIKKVQSFLRGWLCRRKWKNIIQDYIRSPHADSMRKRNQVVFSMLEAEAEYVQQLHILVNNFLRPLRMAASSKKPPITHDDVSSIFLNSETIMFLHQIFYQGLKARIASWPTLVLADLFDILLPMLNIYQEFVRNHQYSLQILAHCKQNRDFDKLLKQYEAKPDCEERTLETFLTYPMFQIPRYILTLHELLAHTPHEHVERNSLDYAKSKLEELSRVMHDEVSETENIRKNLAIERMITEGCEILLDTSQTFVRQGSLIQVPMSEKGKINKGRLGSLSLKKEGERQCFLFSKHLIICTRGSGSKLHLTKNGVISLIDCTLLDDPENMDDDGKGQEVDHLDFKIWVEPKDSPPFTVILVASSRQEKAAWTSDIIQCVDNIRCNGLMMNAFEENSKVTVPQMIKSDASLYCDDVDIRFSKTMNSCKVLQIRYASVERLLERLTDLRFLSIDFLNTFLHSYRVFTDAVVVLDKLISIYKKPITAIPARSLELLFSSSHNTKLLYGDAPKSPRASRKFSSPPPLAIGTSSPVRRRKLSLNIPIITGGKALELASLGCPSDGYTNIHSPISPFGKTTLDTSKLCVASSLTRTPEEIDMTTLEESSGFRKPTSDILKEESDDDQSDVDDTEVSPPTPKSFRNRITQEFPLFNYNSGIMMTCRDLMDSNRSPLSATSAFAIATAGANESPANKEIYRRMSLANTGYSSDQRNIDKEFVIRRAATNRVLNVLRHWVTKHSQDFETDDLLKYKVICFLEEVMHDPDLLPQERKAAANIMRTLTQEEITENHSMLDELLLMTEGVKTEPFENHSAMEIAEQLTLLDHLVFKSIPYEEFFGQGWMKADKNERTPYIMKTTRHFNHISNLIASEILRNEEVSARASTIEKWVAVADICRCLHNYNAVLEITSSINRSAIFRLKKTWLKVSKQTKSLFDKLQKLVSSDGRFKNLRETLRNCDPPCVPYLGMYLTDLAFLEEGTPNYTEDGLVNFSKMRMISHIIREIRQFQQTTYKIEPQPKVTQYLVDETFVLDDESLYEASLRIEPKLPT')

        for seq in data['db_sequences'][:]:
            blast_db.add_sequence(seq)

        # get_seq = blast_db.get_sequences('DEF')
        #
        # stats = blast_db.get_db_stats()

        blast = Blast(data['sub_matrix'])
        # words = blast.get_words(sequence=data['query_seq'], T=13)
        # blast.get_words(pssm=data['query_pssm'], T=11)

        # blast.get_hsp('MGPRARPAFLLLMLLQTAVL', 7, 'MGELMAFLLPLIIVLMVKHS', 6, False, 20, 5)

        results = blast.search_two_hit(blast_db,
                                       query=data['query_seq'],
                                       T=11,
                                       X=5,
                                       S=30,
                                       A=40)

        truth = table_list_tuple(data['blast_hsp_two_hit_1'])

        counter = 0
        for target in results:
            for hsp in results[target]:
                counter += 1

        diff = [x for x in truth if (x not in results)]

        for target in results:
            if set(results[target]) != set(truth[target]):
                print(target)
                print(truth[target])
                print(list(results[target]))

        compare_blast_results(truth, results)

        pass



if __name__ == '__main__':
    main()
